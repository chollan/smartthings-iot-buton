/**
 *  HTTP Toggle
 *
 *  Copyright 2018 Curtis Holland
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 */
definition(
    name: "IoT Button Automation",
    namespace: "chollan",
    author: "Curtis Holland",
    description: "toggle a lamp with IoT",
    category: "My Apps",
    parent: "chollan:IoT Buttons Management",
    iconUrl: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience.png",
    iconX2Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png",
    iconX3Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png"
)

preferences {
    /*section("Off Button: Which items do you want to turn off on the button press?") {
        input "offButtonSwitches", "capability.switch", multiple: true
    }*/
    page(name: "selectButtons", title: "Which button would you like to to control?", nextPage:"doWhat", install: false, uninstall: false){
	    section{
		    input(name: "button", type: "enum", title: "Button", options: getButtonOptions(), required: true)
        }
        section{
		    input(name: "tapType", type: "enum", title: "Click Type", options: ["Single", "Double", "Long"], required: true)
        }
    }
    page(name: "doWhat", title: "Button Action", nextPage:"selectActions", install: false, uninstall: false){
    	section{
        	def buttonActionsMapping = [
	            routine:"Run a Routune",
                off: "Turn devices off",
                on: "Turn devices on",
                toggle: "Toggle the devices"
            ]
		    input(name: "buttonAction", type: "enum", title: "What do you want to do with this switch", options: buttonActionsMapping, required: true)
        }
    }
    page(name: "selectActions", title: "", nextPage:"namePage",  install: false, uninstall: false)
    page(name: "namePage")
    
}

def initialize(){
	if (!overrideLabel) {
        app.updateLabel(defaultLabel())
    }
}

def getButtonOptions(){
	return ["G030-JF05-4254-ACCT"]
	// for now let's not do this
    def params = [
        uri: "https://hl2fcsl3n1.execute-api.us-east-1.amazonaws.com",
        path: "/v1/buttons"
    ]

    try {
        httpGet(params) { resp ->
            resp.headers.each {
                //log.debug "${it.name} : ${it.value}"
            }
            //log.debug "response contentType: ${resp.contentType}"
            //new groovy.json.JsonSlurper().parseText(str)  
            log.debug "response data: ${resp.data}"
            return resp.data
        }
    } catch (e) {
        log.error "something went wrong inside getButtonOptions: $e"
        return ["G030-JF05-4254-ACCT"]
        //return []
    }
    
}

def selectActions() {
    dynamicPage(name: "selectActions", title: "", install: true, uninstall: true) {
		if(buttonAction == "routine"){
            // get the available actions
            def actions = getRoutineMapping()
            if (actions) {
                // sort them alphabetically
                actions.sort()
                section("AWS Good Night Button") {
                    log.trace actions
                    // use the actions as the options for an enum input
                    input "routine", "enum", title: "Select an action to execute", options: actions
                }
            }
        }
        if(buttonAction == "off"){
        	section("Switch these off") {
                input "switches", "capability.switch", title: "Select Select the switches to turn off", multiple: true
            }
        }
        if(buttonAction == "on"){
        	section("Switch these on") {
                input "switches", "capability.switch", title: "Select Select the switches to turn on", multiple: true
            }
        }
        if(buttonAction == "toggle"){
        	section("Toggle these on") {
                input "switches", "capability.switch", title: "Select Select the switches to turn on", multiple: true
            }
        }
    }
}

def namePage() {
    if (!overrideLabel) {
        // if the user selects to not change the label, give a default label
        def l = defaultLabel()
        log.debug "will set default label of $l"
        app.updateLabel(l)
    }
    dynamicPage(name: "namePage", title: "", install: true, uninstall: true) {
        if (overrideLabel) {
            section("Automation name") {
                label title: "Enter custom name", defaultValue: app.label, required: false
            }
        } else {
            section("Automation name") {
                paragraph app.label
            }
        }
        section {
            input "overrideLabel", "bool", title: "Edit automation name", defaultValue: false, required: false, submitOnChange: true
        }
    }
}

def defaultLabel() {
    //def lightsLabel = switches.size() == 1 ? switches[0].displayName : switches[0].displayName + ", etc..."

    if (tapType == "Single") {
        "Single tap ${button}"
    } else if (tapType == "Double") {
        "Double tap ${button}"
    } else {
        "Long press ${button}"
    }
}

def getRoutineMapping(routineId){
	def actions = location.helloHome?.getPhrases()*.label
    if(routineId){
    	try{
	    	return actions.get(routineId as int)
        } catch(Exception ex) {
            return null
        }
    }
    return actions
}

mappings {

  path("/buttonHandler") {
    action: [
      POST: "buttonHandler"
    ]
  }

  path("/switches") {
    // GET requests to /switches endpoint go to listSwitches.
    // PUT requests go to updateSwitches
    action: [
      GET: "getSwitchesHandler",
      PUT: "updateSwitches"
    ]
  }
  
  // GET requests to endpoint /switches/<id> go to getSwitch
  // PUT requests to endpoint /switches/<id> go to updateSwitch
  path("/switches/:id") {
    action: [
        GET: "getSwitchHandler",
        PUT: "updateSwitch"
    ]
  }
}

// return a map in the form of [switchName, switchStatus]
// the returned value will be converted to JSON by the platform
def getSwitchesHandler() {
    def status = [:]
    //log.warn location.helloHome?.getPhrases()
    //def actions = location.helloHome?.getPhrases()*.label
    //log.warn actions
	//location.helloHome?.execute("Good Night!")
    log.warn state.accessToken
    
	theSwitches.each {theSwitch ->
        log.trace "will populate status map"
        log.trace "theSwitch id: ${theSwitch.id}"
        log.debug "${theSwitch.getSupportedCommands()}"
        def switchInfo = ["name":theSwitch.displayName, "status":theSwitch.currentSwitch, "level": theSwitch.currentLevel, "id":theSwitch.id]
        status.put(theSwitch.id, switchInfo)
    }
    
    log.debug "getSwitches returning: $status"
    return status
}

def getSwitchHandler() {
    def theSwitch = theSwitches.find{it.id == params.id}
    def switchInfo = ["name":theSwitch.displayName, "status":theSwitch.currentSwitch, "level": theSwitch.currentLevel]
    [theSwitch.id, switchInfo]
}

// execute the command specified in the request
// returns a 400 error if a non-supported command
// is specified (only on, off, or toggle supported)
// assumes request body with JSON in format {"command" : "<value>"}
def updateSwitches() {
    log.trace "updateSwitches: request: $request"
    log.trace "updateSwitches: params: $params"
    
    theSwitches.each {
        doCommand(it, request.JSON.command)
    }
}

// execute the command specified in the request
// return a 400 error if a non-supported command 
// is specified (only on, off, or toggle supported)
// assumes request body with JSON in format {"command" : "<value>"}
def updateSwitch() {
    log.trace "updateSwitch: look for swithc with id ${params.id}"
    def theSwitch = theSwitches.find{it.id == params.id}
    doCommand(theSwitch, request.JSON.command)
}

def doCommand(theSwitch, command) {
    if (command == "toggle") {
        if (theSwitch.currentSwitch == "on") {
            log.debug "will try and turn switch ${theSwitch.displayName} on"
            theSwitch.off()
        } else {
            log.debug "will try and turn switch ${theSwitch.displayName} off"
            theSwitch.on()
        }
    } else if (command == "on" || command == "off") {
        theSwitch."$command"()
    } else {
        httpError(400, "Unsupported command - only 'toggle', 'off', and 'on' supported")
    }
}

// called when SmartApp is installed
def installed() {
    log.debug "Installed with settings: ${settings}"
    log.debug "app state: ${state}"
    log.warn state.accessToken
    if(!state.accessToken) {
        // the createAccessToken() method will store the access token in state.accessToken
        createAccessToken()
    }
    notifyLambda("install")
    
}

// called when any preferences are changed in this SmartApp. 
def updated() {
    log.debug "Updated with settings: ${settings}"
    unsubscribe()
    log.warn state.accessToken
    if(!state.accessToken) {
        // the createAccessToken() method will store the access token in state.accessToken
        createAccessToken()
    }
    notifyLambda("update")
    log.debug getButtonOptions()
}

def uninstalled(){
	log.debug "Uninstalled with settings: ${settings}"
    unsubscribe()
    notifyLambda("uninstall")
}

def buttonHandler(){
	def type = request.JSON?.type
    if(!type){
    	return [error: true]
    }else{
        log.debug "type: ${type}"
        log.debug "tapType: ${tapType}"
        if(type.toLowerCase() == tapType.toLowerCase()){
	        log.debug "YES!"
            if(buttonAction == "off"){
                switches.each {
                    // check to ensure the switch does have the setLevel command
                    if (it.hasCommand('off')) {
                        log.debug("turning off $it.displayName")
                        it.off()
                    }
                }
            } 

            if(buttonAction == "on"){
                switches.each {
                    // check to ensure the switch does have the setLevel command
                    if (it.hasCommand('on')) {
                        log.debug("turning on $it.displayName")
                        it.on()
                    }
                }        
            }

            if(buttonAction == "toggle"){
                switches.each {
                    def currentValue = it.currentValue("switch")
                    if(currentValue == "on"){
                        it.off()
                    }else{
                        it.on()
                    }
                }        
            }

            if(buttonAction == "routine"){
                //log.debug "executing routine ${routine}"
                def routineString = getRoutineMapping(routine)
                if(routineString){
                    log.debug "executing ${routineString}"
                    location.helloHome?.execute(routineString)
                }else{
                    log.error "unable to find routine"
                }
            }
        }
    }
    return [error: false]
}

def notifyLambda(actionRcvd){
	def jsonPostParams = [
    	uri: "https://hl2fcsl3n1.execute-api.us-east-1.amazonaws.com/v1/app",
        body: [
        	action: actionRcvd,
            token: state.accessToken,
            appId: app.id,
            button: button,
            url: getApiServerUrl()
        ]
    ]
    try {
        httpPostJson(jsonPostParams) { resp ->
            resp.headers.each {
                log.debug "${it.name} : ${it.value}"
            }
            log.debug "response contentType: ${resp.contentType}"
        }
    } catch (e) {
        log.debug "something went wrong inside notifyLambda: $e"
    }
}